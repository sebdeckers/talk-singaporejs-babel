# Babel Plugins

Writing Code That Writes Code

#HSLIDE?image=https://pbs.twimg.com/profile_images/567000326444556290/-1wfGjNw.png

#HSLIDE

# Stages of ~~Babel~~ Any Compiler

1. **Parse** code to AST
1. **Transform** AST to AST
1. **Generate** AST to code

#HSLIDE

# 1. Parse

Turn code into tokens, and tokens into a tree of nodes.

#VSLIDE?image=https://raw.githubusercontent.com/babel/logo/master/babylon.png

#VSLIDE

- **Lexical Analysis**

    Turn source code into *tokens*

- **Syntactic Analysis**

    Turn tokens into *Abstract Syntax Tree (AST)*

#VSLIDE?image=assets/astexplorer.png

# <a href="https://astexplorer.net/" data-preview-link>AST Explorer</a>

#HSLIDE

# 2. Transform

Add, replace, edit, remove nodes in the AST.

#HSLIDE

# 3. Generate

Turn the AST nodes into *source code* and a *source map*.

#HSLIDE

# Let's code!

#HSLIDE

### `babel-traverse`: Visitors, Paths, & Nodes

```js
import {transform} from 'babel-core'
transform('console.log("hello", "world" + "!")', {
  plugins: [{
    visitor: {
      StringLiteral () {
        console.log('found a string!')
      }
    }
  }]
})
```

#VSLIDE

### Use Case: Linting

```js
import {transform} from 'babel-core'
transform('var Foo = 123; var bar = 456;', {
  plugins: [{
    visitor: {
      VariableDeclarator (path) {
        const name = path.node.id.name
        const first = variableName.at(0)
        if (first === first.toLocaleUpperCase()) {
          console.warn(`"${name}" ` +
            `begins with an uppercase letter.`)
        }
      }
    }
  }]
})
```

#HSLIDE

### `babel-types`: Validators & Builders

Helper methods for all AST node types.

Example for StringLiteral type:
```js
// Validate a node as StringLiteral
t.isStringLiteral(node, opts)

// Build a new StringLiteral
t.stringLiteral(value)
```

[github.com/babel/babel/tree/master/packages/babel-types](https://github.com/babel/babel/tree/master/packages/babel-types)

#VSLIDE

### Use Case: Transpiling

```js
import {transform} from 'babel-core'
const {code} = transform('alert("hello world")', {
  plugins: [({types: t}) => ({
    visitor: {
      CallExpression (path) {
        if (t.isIdentifier(path.node.callee, {name: 'alert'})) {
          path.get('callee').replaceWith(
            t.memberExpression(
              t.identifier('console'),
              t.identifier('log'))
          )
        }
      }
    }
  })]
})
console.log(code)
```

#HSLIDE

### Scopes

Create a new variable name that doesn't clash
```js
path.scope.generateUidIdentifier('foo')
// Node {type: 'Identifier', name: 'foo1'}
```

Rename a variable to something unique
```js 
path.scope.rename('foo', 'bar')
//  in: var foo = 123;
// out: var bar = 123;
```

#VSLIDE

### Use Case: Minifying

```js
import {transform} from 'babel-core'
const source =
  `function incrementVariable (somethingReallyLong) {
    return somethingReallyLong + 1 }`
const {code} = transform(source, {
  plugins: [{
    visitor: {
      Identifier (path) {
        if (path.node.name.length > 2) {
          path.scope.rename(
            path.node.name,
            path.scope.generateUidIdentifier('_').name
          )
        }
      }
    }
  }]
})
console.log(code)
```

#HSLIDE

Questions? RTFM

# <a href="https://github.com/thejameskyle/babel-handbook/blob/master/translations/en/plugin-handbook.md" target="_blank">Babel Plugin Handbook</a>

#HSLIDE

### Here's what I'm doing with this stuff...

#VSLIDE

#### <a href="https://www.npmjs.com/package/babel-plugin-transform-import-resolve" target="_blank">transform-import-resolve</a>

Resolve import paths for named dependencies

```js
// in:
import derp from 'derp'
// out:
import derp from './node_modules/derp/lib/index.js'
```

#VSLIDE

#### <a href="https://www.npmjs.com/package/babel-plugin-transform-commonjs-es2015-modules" target="_blank">transform-commonjs-es2015-modules</a>

CommonJS to ES2015 modules (export/import) transformation plugin for Babel

```js
// in:
var foo = require('bar');
// out:
import foo from 'bar';
```

#VSLIDE

#### <a href="https://www.npmjs.com/package/unbundle" target="_blank">Unbundle</a>

require() and import/export in the browser, without the bundling

```js
unbundle --watch --entry ./src/app.js --destination ./dist
```

#HSLIDE

![Sebastiaan profile photo](https://gitlab.com/uploads/user/avatar/400681/avatar.png)

# <a href="https://gitlab.com/sebdeckers" target="_blank" style="text-transform: none;">@sebdeckers</a>
