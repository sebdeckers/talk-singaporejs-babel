import {transform} from 'babel-core'
transform('console.log("hello", "world" + "!")', {
  plugins: [{
    visitor: {
      StringLiteral () {
        console.log('found a string!')
      }
    }
  }]
})
